
#include "stdafx.h"
#include <iostream>
#include <unordered_map>
using namespace std;

class TestClass
{
public:
	void loop_map(unordered_map<int, int> hash);
	int find_map(unordered_map<int, int> hash, int key);
	void find_map(unordered_map<int, int> hash, int key, int &value);
};

void TestClass::loop_map(unordered_map<int, int> hash)
{
	cout << "Looping through map and printing key,value pairs:\n";

	for (auto pair : hash)
	{
		cout << pair.first << " " << pair.second << "\n";
	}
}

int TestClass::find_map(unordered_map<int, int> hash, int key)
{
	cout << "Attempting to find value by key, will return -1 if the key could not be found:\n";

	auto it = hash.find(key);
	if (it != hash.end())
		return it->second;
	else
		return -1;
}

void TestClass::find_map(unordered_map<int, int> hash, int key, int &value)
{
	cout << "Attempting to find value by key, will return -1 if the key could not be found:\n";

	auto it = hash.find(key);
	if (it != hash.end())
		value = it->second;
	else
		value = -1;
}

int main() {

	TestClass *tc = new TestClass();

	unordered_map<int, int> hash;

	hash.insert({ 1,2 });
	hash.insert({ 2,4 });
	hash.insert({ 3,6 });
	hash.insert({ 4,8 });

	tc->loop_map(hash);
	cout << tc->find_map(hash, 1) << "\n";
	cout << tc->find_map(hash, 5) << "\n";

	int value;
	tc->find_map(hash, 2, value);
	cout << value << "\n";
	tc->find_map(hash, 6, value);
	cout << value << "\n";

	delete tc;

	return 0;
}