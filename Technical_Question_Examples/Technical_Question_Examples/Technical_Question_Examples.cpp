// Technical_Question_Examples.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
// find_cube_pairs
#include <cmath>
#include <ctgmath>
#include <unordered_map>
// for std::sort
#include <algorithm>
// test_hash
#include <functional>
// test_stringstream
#include <sstream>

using namespace std;

/*
Test:
Print all positive integer solutions to the equation a^3 + b^3 = c^3 + d^3
a,b,c,d = integers from 1 to 1000
*/
void find_cube_pairs()
{
#define pow_cubed(i) (int)pow(i, 3)

	int min = 1;
	int max = 50;  // simplified to see result

	// brute force way - O(n^4)
	// loop through 1 through max four times, doing a calculation at the end to see if we satisfy the equation
	//for (int a = min; a <= max; a++)
	//{
	//	for (int b = min; b <= max; b++)
	//	{
	//		for (int c = min; c <= max; c++)
	//		{
	//			for (int d = min; d <= max; d++)
	//			{
	//				if (pow_cubed(a) + pow_cubed(b) == pow_cubed(c) + pow_cubed(d))
	//				{
	//					printf("(%d, %d) = (%d, %d)\n",a,b,c,d);
	//					break;
	//				}
	//			}
	//		}
	//	}
	//}

	// brute force way, optimized - O(n^3)
	// there only exists one d = pow(a^3 + b^3 - c^3, 1/3), so solve for that and print that
	//for (int a = min; a <= max; a++)
	//{
	//	for (int b = min; b <= max; b++)
	//	{
	//		for (int c = min; c <= max; c++)
	//		{
	//			int d = pow(pow_cubed(a) + pow_cubed(b) - pow_cubed(c), (1 / 3));
	//			if (pow_cubed(a) + pow_cubed(b) == pow_cubed(c) + pow_cubed(d))
	//			{
	//				printf("(%d, %d) = (%d, %d)\n", a, b, c, d);
	//			}
	//		}
	//	}
	//}

	// optimized - O(n^2)
	unordered_map<int, pair<int, int>> hash;

	for (int a = min; a <= max; a++)
	{
		for (int b = a; b <= max; b++)
		{
			int result = pow_cubed(a) + pow_cubed(b);

			hash.insert({ result, pair<int,int>({ a,b }) });
		}
	}

	for (auto result : hash)
	{
		printf("(%d, %d) = (%d, %d)\n", result.second.first, result.second.second, result.second.second, result.second.first);
	}
}

/*
Test:
Implement and algorithm to determine if a string has all unique characters.
Beyond that: What if you cannot use aditional data structures?
-
Questions to ask:
Is the string Unicode or ASCII?
-
Assume it is an ASCII string (128 possible characters)
*/
bool find_if_string_has_unique_characters(string str)
{
	// can't be unique if it contains more than the possible set of ASCII characters
	if (str.length() > 128)
	{
		return false;
	}

	// Brute Force Method - slightly less than O(N^2)

	//for (size_t i = 0; i < str.length(); i++)
	//{
	//	for (size_t j = i+1; j < str.length(); j++)
	//	{
	//		if (str[i] == str[j])
	//		{
	//			return false;
	//		}
	//	}
	//}
	// return true;

	// Hash Method - O(N) - N is the length of the string
	// Best case - we find a matching character and return
	// Worst case - we do not find a matching character and go through the entire string

	//unordered_map<size_t, char> map;
	//hash<char> str_hash;
	//for (size_t i = 0; i < str.length(); i++)
	//{
	//	map.insert({ str_hash(str[i]),str[i] });
	//	if (map.size() != i + 1)
	//	{
	//		return false;
	//	}
	//}

	 //Boolean Array Method - O(N)

	bool bool_arr[128]{};
	for (size_t i = 0; i < str.length(); i++)
	{
		auto b = bool_arr[str[i]];
		if (bool_arr[str[i]])
		{
			return false;
		}
		bool_arr[str[i]] = true;
	}

	// Sorting Method
	// WARNING - takes more space

	//sort(str.begin(), str.end());
	//for (size_t i = 0; i < str.length() - 1; i++)
	//{
	//	if (str[i] == str[i + 1])
	//	{
	//		return false;
	//	}
	//}

	return true;
}

/*
Test:
Given two strings, write a method to determine if one is a permutation of the other
-
Questions to ask:
Are the strings unicode or ASCII?
Does whitespace matter?
Does case sensitivity matter?
-
Assume they are ASCII strings (128 possible characters) with case sensitivity
*/
bool find_if_string_is_permutation(string first, string second)
{
	if (first.size() != second.size())
	{
		return false;
	}
	// we know that the two strings are the same size
	auto size = first.size();

	// does case matter?  i.e. is Dog a permutation of god?
	// if so (and keep in mind that this adds to big O):
	//transform(first.begin(), first.end(), first.begin(), ::tolower);
	//transform(second.begin(), second.end(), second.begin(), ::tolower);

	// Sort Method - O(N log N)

	//sort(first.begin(), first.end());
	//sort(second.begin(), second.end());
	//return (first == second);

	// Hash Method - O(N)

	//unordered_map<size_t, char> map;
	//hash<char> char_hash;
	//for (size_t i = 0; i < size; i++)
	//{
	//	map.insert({ char_hash(first[i]), first[i] });
	//	map.insert({ char_hash(second[i]), second[i] });
	//}
	//return (map.size() == size);

	// Int Array Method - O(N)
	// The idea is that we have an array with all possible ASCII characters (represented as ints)
	// and we increment/decrement the values for each char in the string
	// if we have any negative values after looping the second time, we have found a new character, return false
	// otherwise we have an array of all zeroes at the end, return true

	int letters[128]{};
	for (size_t i = 0; i < size; i++)
	{
		letters[first[i]]++;
	}

	for (size_t i = 0; i < size; i++)
	{
		letters[second[i]]--;
		if (letters[second[i]] < 0)
		{
			return false;
		}
	}
	return true; // no negative values, therefore no positive values either
}

/*
Test:
You are given two sorted arrays, A and B, where A has a large enough buffer at the end to hold B.
Write a method to merge B into A in sorted order
-
Questions to ask:
What type of variable is held in the arrays?
How are they sorted?
-
Assume ints are sorted in ascending value (i.e. 1,2,3,4...)
*/

void merge_two_sorted_arrays(int A[], int B[], int count_a, int count_b)
{
	int index_merged = count_a + count_b - 1;	// the index we will add to
	int index_a = count_a - 1;					// last element of A
	int index_b = count_b - 1;					// last element of B

	while (index_b >= 0) // loop until we're out of B elements, since we're adding those to A
	{
		if (index_a >= 0 && A[index_a] > B[index_b]) // if A[...] > B[...]
		{
			A[index_merged] = A[index_a];	// add the A[...] element to the end
			index_a--;
		}
		else
		{
			A[index_merged] = B[index_b];	// add the B[...] element to the end
			index_b--;
		}
		index_merged--;
	}
}

void test_hash()
{
	char nts1[] = "Test";
	char nts2[] = "Test";
	char nts3[] = "test";
	string str1(nts1);
	string str2(nts2);
	string str3(nts3);

	std::hash<char*> ptr_hash;
	std::hash<string> str_hash;

	cout << "same hashes:\n" << boolalpha;
	auto nts1_hash = ptr_hash(nts1);
	auto nts2_hash = ptr_hash(nts2);
	auto str1_hash = str_hash(str1);
	auto str2_hash = str_hash(str2);
	// different hashes for the char*[]
	cout << (nts1_hash == nts2_hash) << endl;
	// same hashes for the std::string
	cout << (str1_hash == str2_hash) << endl;

	// different hash for std::string, since the "t"s are different cases
	auto str3_hash = str_hash(str3);
	cout << (str1_hash == str3_hash) << endl;
}

void test_stringstream()
{
	stringstream ss1;
	ss1 << 100 << ' ' << 200;

	int foo, bar;
	ss1 >> foo >> bar;

	cout << "foo: " << foo << endl;
	cout << "bar: " << bar << endl;

	// construct a string from an array of strings
	string str_arr[] = { "Hello,", "how", "are", "you?" };

	stringstream ss2;
	// size of string array is
	// (sizeof(array)/sizeof(array[0]))
	// MEMORIZE THIS
	auto size = sizeof(str_arr) / sizeof(str_arr[0]);
	for (size_t i = 0; i < size; i++)
	{
		ss2 << str_arr[i] << " ";
	}

	cout << ss2.str() << endl;

	// same as above, but with a vector instead
	vector<string> str_vect = { "I", "am", "well,", "thank", "you." };

	stringstream ss3;
	for (auto it = str_vect.begin(); it != str_vect.end(); ++it)
	{
		ss3 << *it << " ";
	}

	cout << ss3.str() << endl;
}

/*********************************
Quick Sort
*/

/*
This function takes last element as pivot, places
	the pivot element at its correct position in sorted
	array, and places all smaller (smaller than pivot)
	to left of pivot and all greater elements to right
	of pivot
*/
int qs_partition(int arr[], int low, int high)
{
	// use the high index as the pivot
	int pivot = arr[high];
	// index of the smaller element
	int low_index = (low - 1);

	for (int j = low; j <= high-1; j++)
	{
		if (arr[j] <= pivot)
		{
			// increment low_index and swap arr[low_index] and arr[j]
			low_index++;
			swap(arr[low_index], arr[j]);
		}
	}
	// swap arr[low_index + 1] and arr[high]
	swap(arr[low_index + 1], arr[high]);
	return (low_index + 1);
}

void quicksort(int arr[], int low, int high)
{
	if (low < high)
	{
		// pi is partitioning index, arr[pi] is now at the right place
		int pi = qs_partition(arr, low, high);

		quicksort(arr, low, pi - 1);
		quicksort(arr, pi + 1, high);
	}
}

/*********************************
Merge Sort
*/

void merge(int arr[], int helper[], int low, int middle, int high)
{
	// copy both halves into the helper array
	for (int i = low; i <= high; i++)
	{
		helper[i] = arr[i];
	}

	int helper_left = low;
	int helper_right = middle + 1;
	int current = low;

	// iterate through the helper array, compare the left and right half, and
	// copy back the samller element from the two halves into the original array
	while (helper_left <= middle && helper_right <= high)
	{
		// if left <= right
		if (helper[helper_left] <= helper[helper_right])
		{
			arr[current] = helper[helper_left];
			helper_left++;
		}
		// if right < left
		else
		{
			arr[current] = helper[helper_right];
			helper_right++;
		}
		current++;
	}

	// copy the rest of the left side of helper into arr
	// don't need to copy the right side, it's already there
	int remaining = middle - helper_left;
	for (int i = 0; i <= remaining; i++)
	{
		arr[current + i] = helper[helper_left + i];
	}
}

void mergesort(int arr[], int helper[], int low, int high)
{
	if (low < high)
	{
		int middle = low + (high - low) / 2;
		mergesort(arr, helper, low, middle);		// sort the left half
		mergesort(arr, helper, middle + 1, high);	// sort the right half
		merge(arr, helper, low, middle, high);		// merge them
	}
}

void mergesort(int arr[], int size)
{
	int* helper = new int[size];
	mergesort(arr, helper, 0, size - 1);

	delete helper;
}

void print_array(int arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("%d ", arr[i]);
	}
}

int main()
{
	int A[8] = { 1,2,3,4,5 };
	int B[3] = { 1,4,8 };

	merge_two_sorted_arrays(A, B, 5, 3);

	print_array(A, 8);

	return 0;
}